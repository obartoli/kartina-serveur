package com.formation.kartina.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.mail.MailException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.formation.kartina.models.Commande;
import com.formation.kartina.models.Format;
import com.formation.kartina.models.Photographie;
import com.formation.kartina.models.Theme;
import com.formation.kartina.models.Utilisateur;
import com.formation.kartina.models.Vente;
import com.formation.kartina.services.CommandeService;
import com.formation.kartina.services.FormatService;
import com.formation.kartina.services.PhotographieService;
import com.formation.kartina.services.ThemeService;
import com.formation.kartina.services.UtilisateurService;
import com.formation.kartina.services.VenteService;
import com.formation.kartina.utils.Mailer;


@RestController
@RequestMapping("/api")
public class UtilisateurController {

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private VenteService venteService;

	@Autowired
	private ThemeService themeService;

	@Autowired
	private PhotographieService photoService;

	@Autowired
	private FormatService formatService;

	@Autowired
	private CommandeService commandeService;

	@Autowired 
	private Mailer mailer;

	@GetMapping("/user/{id}")
	public Optional<Utilisateur> getUser(@PathVariable(name="id") Integer id) {
		return this.utilisateurService.findById(id);
	}

	@GetMapping("/users")
	@Secured({"ROLE_ADMIN"})
	public List<Utilisateur> getUsers() {
		return this.utilisateurService.findAll();
	}

	@PostMapping("/user")
	public Utilisateur addUser(@RequestBody Utilisateur utilisateur) {
		return this.utilisateurService.inscription(utilisateur);
	}

	@PutMapping("/user")
	public Utilisateur updateUser(@RequestBody Utilisateur utilisateur) {
		return this.utilisateurService.update(utilisateur);
	}

	@PostMapping("/addvente")
	public Vente addVente(@RequestBody Vente vente) {
		/*Photographie photo =
				photoService.store(file);*/
		vente.setDateDebut(LocalDate.now());
		//vente.getPhotographie().setOrientation(photo.getOrientation());
		//vente.getPhotographie().setUrlImage(photo.getUrlImage());
		vente.setUtilisateur(vente.getUtilisateur());
		vente.getPhotographie().setVente(vente);
		return this.venteService.createOrUpdate(vente);
	}
	

	List<String> files = new ArrayList<String>();

	 @PostMapping("/post")
	  public Photographie handleFileUpload(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    try {
	      Photographie photo = photoService.store(file);
	      photo.setFormats(photo.getFormats());
	      photo.setThemes(photo.getThemes());
	      photo.setUrlImage(photo.getUrlImage());
	      photo.setOrientation(photo.getOrientation());
	      files.add(file.getOriginalFilename());
	 
	      message = "You successfully uploaded " + file.getOriginalFilename() + "!";
	      return photo;
	    } catch (Exception e) {
	      message = "FAIL to upload " + file.getOriginalFilename() + "!";
	      return new Photographie();
	    }
	  }
	
	@PutMapping("/editerbio")
	public Utilisateur editerBio(@RequestBody Utilisateur utilisateur) {
		return this.utilisateurService.createOrUpdate(utilisateur);
	}

	@DeleteMapping("/user")
	public void deleteUser(@RequestBody Utilisateur utilisateur) {
		this.utilisateurService.delete(utilisateur);
	}

	@GetMapping("/bestvente")
	public Page<Vente> getBestVente() {
		return this.venteService.getBestVente(PageRequest.of(0, 6));
	}

	@GetMapping("/newvente")
	public List<Vente> getNewVente() {
		List<Vente> lv = this.venteService.getNewVente(PageRequest.of(0, 24));
		return lv;
	}

	//Ajout pageable
	@GetMapping("/venteutilisateur/{id}")
	public Page<Vente> getVenteByUtilisateur(@PathVariable(name="id")Integer id, @RequestParam(name = "page", defaultValue = "0") int page) {
		Page<Vente> lv = this.venteService.getVenteByUtilisateur(id, PageRequest.of(page, 24));
		return lv;
	}

	@GetMapping("/typeutilisateur")
	public List<Utilisateur> getArtiste() {
		List<Utilisateur> lu = this.utilisateurService.getArtiste(2);
		return lu;
	}

	@GetMapping("/sell")
	public Page<Vente> getCurrentSell(@RequestParam(name = "page", defaultValue = "0") int page) {
		Page<Vente> lv = this.venteService.findAll(PageRequest.of(page, 24));
		return lv;
	}

	@GetMapping("/getthemes")
	public List<Theme> getAllThemes() {
		return this.themeService.findAll();
	}

	@GetMapping("/getbythemes/{id}")
	public Page<Vente> getByThemes(@PathVariable(name="id")Integer id, @RequestParam(name = "page", defaultValue = "0") int page) {
		return this.venteService.findByTheme(id, PageRequest.of(page, 24));
	}

	@GetMapping("/moncompte/{email}")
	public Optional<Utilisateur> findByUsername(@PathVariable(name="email") String email) {
		return this.utilisateurService.findByUsername(email);
	}

	@GetMapping("/ventecours/{id}")
	Page<Vente> getVenteCours(@PathVariable(name="id")Integer id, @RequestParam(name = "page", defaultValue = "0") int page) {
		Page<Vente> lv = this.venteService.getVenteCours(id, PageRequest.of(page, 6));
		return lv;
	}

	@GetMapping("/ventepassee/{id}")
	Page<Vente> getVentePassee(@PathVariable(name="id")Integer id, @RequestParam(name = "page", defaultValue = "0") int page) {
		Page<Vente> lv = this.venteService.getVentePassee(id, PageRequest.of(page, 6));
		return lv;
	}

	@GetMapping("/sell/lastcopies/")
	public Page<Vente> getLastCopies(@RequestParam(name = "page", defaultValue = "0") int page) {
		return this.venteService.findLastCopies(PageRequest.of(page, 24));
	}

	@GetMapping("/sell/new/random")
	public Vente getRandomNewSell() {
		List<Vente> lstSells = this.venteService.getNewVente(PageRequest.of(0, Integer.MAX_VALUE));
		return this.venteService.getRandomVente(lstSells);
	}

	@GetMapping("/sell/theme/{id}/random")
	public Vente getRandomThemeSell(@PathVariable(name="id")Integer id) {
		Page<Vente> lstSellsByTheme = this.venteService.findByTheme(id, PageRequest.of(0, Integer.MAX_VALUE));
		return this.venteService.getRandomVente(lstSellsByTheme.getContent());
	}

	//Ajout pageable
	@GetMapping("/sell/user/{id}/random")
	public Vente getRandomUserSell(@PathVariable(name="id") Integer id, @RequestParam(name = "page", defaultValue = "0") int page) {
		Page<Vente> lstSellsByUser = this.venteService.getVenteByUtilisateur(id, PageRequest.of(0, Integer.MAX_VALUE));
		return this.venteService.getRandomVente(lstSellsByUser.getContent());
	}

	@GetMapping("/sell/lastcopies/random")
	public Vente getRandomLastCopies() {
		Page<Vente> lstLastCopies = this.venteService.findLastCopies(PageRequest.of(0, Integer.MAX_VALUE));
		return this.venteService.getRandomVente(lstLastCopies.getContent());
	}

	@GetMapping("/photo/orientations")
	public List<String> getOrientions() {
		return this.photoService.findDistinctOrientation();
	}

	@GetMapping("/format/names")
	public List<String> getFormatNames() {
		return this.formatService.findDistinctFormatName();
	}

	@GetMapping("/theme/names")
	public List<String> getThemeNames() {
		return this.themeService.findDistinctName();
	}

	@GetMapping("sell/current")
	public Page<Vente> getSellWithCriterias(@RequestParam(name="orientations", defaultValue = "") List<String> orientations, 
			@RequestParam(name="formats", defaultValue = "") List<String> formats,
			@RequestParam(name="themes", defaultValue = "") List<String> themes,
			@RequestParam(name="page", defaultValue = "0") int page) {
		return this.venteService.findWithCriteria(orientations, formats, themes, PageRequest.of(page, 24));
	}

	@GetMapping("order/all")
	public Page<Commande> getAllOrders(@RequestParam(name="page", defaultValue = "0") int page) {
		return this.commandeService.findAll(PageRequest.of(page, 30));
	}

	@PostMapping("order")
	public Commande createOrder(@RequestBody Commande commande) {
		
		Commande commandeCreee = this.commandeService.createOrder(commande);
		
		try {
			mailer.sendMailOrder(commandeCreee);
		} catch (MailException e){ 
			System.out.println("error sending email : " + e.getMessage());
		}
		
		return commandeCreee;
	}

	@GetMapping("/getformat")
	public List<Format> findAll()
	{
		return this.formatService.findAll();
	}

	@GetMapping("/sell/{id}")
	public Optional<Vente> getVenteById(@PathVariable(name="id") Integer id) {
		return this.venteService.getVenteById(id);
	}

	//test pour l'envoie de l'email
	@PostMapping("/sendemail")
	public String sendEmail(@RequestBody Commande commande) {
		try {
			
			mailer.sendMailOrder(commande);
			
		} catch (MailException e){ 
			System.out.println("error sending email : " + e.getMessage());
		}
		
		return "Email sent successfully";
	} 
}
