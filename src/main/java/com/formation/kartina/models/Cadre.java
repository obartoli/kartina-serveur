package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Cadre database table.
 * 
 */
@Entity
public class Cadre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cadre_id")
	private int cadreId;

	@Column(name="description_cadre")
	private String descriptionCadre;

	@Column(name="majoration_cadre")
	private float majorationCadre;

	@Column(name="nom_cadre")
	private String nomCadre;

	//bi-directional many-to-one association to Ligne_Commande
	@JsonIgnore
	@OneToMany(mappedBy="cadre")
	private List<LigneCommande> ligneCommandes;

	//bi-directional many-to-many association to Finition
	@JsonIgnore
	@ManyToMany
	@JoinTable(
		name="Cadre_has_Finition"
		, joinColumns={
			@JoinColumn(name="cadre_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="finition_id")
			}
		)
	private List<Finition> finitions;

	public Cadre() {
	}

	public int getCadreId() {
		return this.cadreId;
	}

	public void setCadreId(int cadreId) {
		this.cadreId = cadreId;
	}

	public String getDescriptionCadre() {
		return this.descriptionCadre;
	}

	public void setDescriptionCadre(String descriptionCadre) {
		this.descriptionCadre = descriptionCadre;
	}

	public float getMajorationCadre() {
		return this.majorationCadre;
	}

	public void setMajorationCadre(float majorationCadre) {
		this.majorationCadre = majorationCadre;
	}

	public String getNomCadre() {
		return this.nomCadre;
	}

	public void setNomCadre(String nomCadre) {
		this.nomCadre = nomCadre;
	}

	public List<LigneCommande> getLigneCommandes() {
		return this.ligneCommandes;
	}

	public void setLigneCommandes(List<LigneCommande> ligneCommandes) {
		this.ligneCommandes = ligneCommandes;
	}

	public LigneCommande addLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().add(ligneCommande);
		ligneCommande.setCadre(this);

		return ligneCommande;
	}

	public LigneCommande removeLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().remove(ligneCommande);
		ligneCommande.setCadre(null);

		return ligneCommande;
	}

	public List<Finition> getFinitions() {
		return this.finitions;
	}

	public void setFinitions(List<Finition> finitions) {
		this.finitions = finitions;
	}

}