package com.formation.kartina.models;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Commande database table.
 * 
 */
@Entity
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="commande_id")
	private int commandeId;

	@Column(name="date_commande")
	private LocalDate dateCommande;

	@Column(name="numero_facture")
	private String numeroFacture;

	@Column(name="prix_ttc")
	private float prixTtc;

	//bi-directional many-to-one association to Statut_Commande
	@ManyToOne
	@JoinColumn(name="statut_commande_id")
	private StatutCommande statutCommande;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="utilisateur_id")
	private Utilisateur utilisateur;

	//bi-directional many-to-one association to Ligne_Commande
	@OneToMany(mappedBy="commande", cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH})
	private List<LigneCommande> ligneCommandes;

	public Commande() {
	}

	public int getCommandeId() {
		return this.commandeId;
	}

	public void setCommandeId(int commandeId) {
		this.commandeId = commandeId;
	}

	public LocalDate getDateCommande() {
		return this.dateCommande;
	}

	public void setDateCommande(LocalDate dateCommande) {
		this.dateCommande = dateCommande;
	}

	public String getNumeroFacture() {
		return this.numeroFacture;
	}

	public void setNumeroFacture(String numeroFacture) {
		this.numeroFacture = numeroFacture;
	}

	public float getPrixTtc() {
		return this.prixTtc;
	}

	public void setPrixTtc(float prixTtc) {
		this.prixTtc = prixTtc;
	}

	public StatutCommande getStatutCommande() {
		return this.statutCommande;
	}

	public void setStatutCommande(StatutCommande statutCommande) {
		this.statutCommande = statutCommande;
	}

	public Utilisateur getUtilisateur() {
		return this.utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<LigneCommande> getLigneCommandes() {
		return this.ligneCommandes;
	}

	public void setLigneCommandes(List<LigneCommande> ligneCommandes) {
		this.ligneCommandes = ligneCommandes;
	}

	public LigneCommande addLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().add(ligneCommande);
		ligneCommande.setCommande(this);

		return ligneCommande;
	}

	public LigneCommande removeLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().remove(ligneCommande);
		ligneCommande.setCommande(null);

		return ligneCommande;
	}

}