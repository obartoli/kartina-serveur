package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Finition database table.
 * 
 */
@Entity
public class Finition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="finition_id")
	private int finitionId;

	@Column(name="description_finition")
	private String descriptionFinition;

	private float majoration;

	@Column(name="nom_finition")
	private String nomFinition;

	//bi-directional many-to-one association to Ligne_Commande
	@JsonIgnore
	@OneToMany(mappedBy="finition")
	private List<LigneCommande> ligneCommandes;

	//bi-directional many-to-many association to Format
	@JsonIgnore
	@ManyToMany(mappedBy="finitions")
	private List<Format> formats;

	//bi-directional many-to-many association to Cadre
	
	@ManyToMany(mappedBy="finitions")
	private List<Cadre> cadres;

	public Finition() {
	}

	public int getFinitionId() {
		return this.finitionId;
	}

	public void setFinitionId(int finitionId) {
		this.finitionId = finitionId;
	}

	public String getDescriptionFinition() {
		return this.descriptionFinition;
	}

	public void setDescriptionFinition(String descriptionFinition) {
		this.descriptionFinition = descriptionFinition;
	}

	public float getMajoration() {
		return this.majoration;
	}

	public void setMajoration(float majoration) {
		this.majoration = majoration;
	}

	public String getNomFinition() {
		return this.nomFinition;
	}

	public void setNomFinition(String nomFinition) {
		this.nomFinition = nomFinition;
	}

	public List<LigneCommande> getLigneCommandes() {
		return this.ligneCommandes;
	}

	public void setLigneCommandes(List<LigneCommande> ligneCommandes) {
		this.ligneCommandes = ligneCommandes;
	}

	public LigneCommande addLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().add(ligneCommande);
		ligneCommande.setFinition(this);

		return ligneCommande;
	}

	public LigneCommande removeLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().remove(ligneCommande);
		ligneCommande.setFinition(null);

		return ligneCommande;
	}

	public List<Format> getFormats() {
		return this.formats;
	}

	public void setFormats(List<Format> formats) {
		this.formats = formats;
	}

	public List<Cadre> getCadres() {
		return this.cadres;
	}

	public void setCadres(List<Cadre> cadres) {
		this.cadres = cadres;
	}

}