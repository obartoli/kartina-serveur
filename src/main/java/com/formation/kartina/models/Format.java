package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Format database table.
 * 
 */
@Entity
public class Format implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="format_id")
	private int formatId;

	private float majoration;

	@Column(name="nom_format")
	private String nomFormat;

	@Column(name="taille_format")
	private String tailleFormat;

	//bi-directional many-to-one association to Ligne_Commande
	@JsonIgnore
	@OneToMany(mappedBy="format")
	private List<LigneCommande> ligneCommandes;

	//bi-directional many-to-many association to Photographie
	@JsonIgnore
	@ManyToMany(mappedBy="formats")
	private List<Photographie> photographies;

	//bi-directional many-to-many association to Finition
	@ManyToMany
	@JoinTable(
		name="Format_has_Finition"
		, joinColumns={
			@JoinColumn(name="format_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="finition_id")
			}
		)
	private List<Finition> finitions;

	public Format() {
	}

	public int getFormatId() {
		return this.formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	public float getMajoration() {
		return this.majoration;
	}

	public void setMajoration(float majoration) {
		this.majoration = majoration;
	}

	public String getNomFormat() {
		return this.nomFormat;
	}

	public void setNomFormat(String nomFormat) {
		this.nomFormat = nomFormat;
	}

	public String getTailleFormat() {
		return this.tailleFormat;
	}

	public void setTailleFormat(String tailleFormat) {
		this.tailleFormat = tailleFormat;
	}

	public List<LigneCommande> getLigneCommandes() {
		return this.ligneCommandes;
	}

	public void setLigneCommandes(List<LigneCommande> ligneCommandes) {
		this.ligneCommandes = ligneCommandes;
	}

	public LigneCommande addLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().add(ligneCommande);
		ligneCommande.setFormat(this);

		return ligneCommande;
	}

	public LigneCommande removeLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().remove(ligneCommande);
		ligneCommande.setFormat(null);

		return ligneCommande;
	}

	public List<Photographie> getPhotographies() {
		return this.photographies;
	}

	public void setPhotographies(List<Photographie> photographies) {
		this.photographies = photographies;
	}

	public List<Finition> getFinitions() {
		return this.finitions;
	}

	public void setFinitions(List<Finition> finitions) {
		this.finitions = finitions;
	}

}