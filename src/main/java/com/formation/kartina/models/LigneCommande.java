package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the Ligne_Commande database table.
 * 
 */
@Entity
@Table(name = "Ligne_Commande")
public class LigneCommande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ligne_commande_id")
	private int ligneCommandeId;

	@Column(name="prix_unitaire")
	private float prixUnitaire;

	private int quantite;

	//bi-directional many-to-one association to Cadre
	@ManyToOne
	@JoinColumn(name="cadre_id")
	private Cadre cadre;

	//bi-directional many-to-one association to Commande
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="commande_id")
	private Commande commande;

	//bi-directional many-to-one association to Finition
	@ManyToOne
	@JoinColumn(name="finition_id")
	private Finition finition;

	//bi-directional many-to-one association to Format
	@ManyToOne
	@JoinColumn(name="format_id")
	private Format format;

	//bi-directional many-to-one association to Photographie
	@ManyToOne
	@JoinColumn(name="vente_id")
	private Vente vente;

	public LigneCommande() {
	}

	public int getLigneCommandeId() {
		return this.ligneCommandeId;
	}

	public void setLigneCommandeId(int ligneCommandeId) {
		this.ligneCommandeId = ligneCommandeId;
	}

	public float getPrixUnitaire() {
		return this.prixUnitaire;
	}

	public void setPrixUnitaire(float prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public int getQuantite() {
		return this.quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public Cadre getCadre() {
		return this.cadre;
	}

	public void setCadre(Cadre cadre) {
		this.cadre = cadre;
	}

	public Commande getCommande() {
		return this.commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	public Finition getFinition() {
		return this.finition;
	}

	public void setFinition(Finition finition) {
		this.finition = finition;
	}

	public Format getFormat() {
		return this.format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}

	public Vente getVente() {
		return this.vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

}