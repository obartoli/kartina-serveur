package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Photographie database table.
 * 
 */
@Entity
public class Photographie implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="photographie_id")
	private int photographieId;

	private String orientation;

	@Column(name="prix_photo")
	private float prixPhoto;

	private String titre;

	@Column(name="url_image")
	private String urlImage;

	@Column(name="url_miniature")
	private String urlMiniature;

	//bi-directional one-to-one association to Vente
	@JsonIgnore
	@OneToOne(mappedBy="photographie")
	private Vente vente;

	//bi-directional many-to-many association to Tag
	@ManyToMany
	@JoinTable(
		name="Photographie_has_tags"
		, joinColumns={
			@JoinColumn(name="photographie_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="tags_id")
			}
		)
	private List<Tag> tags;

	//bi-directional many-to-many association to Theme
	
	@ManyToMany
	@JoinTable(
		name="Photographie_has_Theme"
		, joinColumns={
			@JoinColumn(name="photographie_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="theme_id")
			}
		)
	private List<Theme> themes;

	//bi-directional many-to-many association to Format
	@ManyToMany
	@JoinTable(
		name="Photographie_has_Format"
		, joinColumns={
			@JoinColumn(name="photographie_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="format_id")
			}
		)
	private List<Format> formats;

	public Photographie() {
	}

	public int getPhotographieId() {
		return this.photographieId;
	}

	public void setPhotographieId(int photographieId) {
		this.photographieId = photographieId;
	}

	public String getOrientation() {
		return this.orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public float getPrixPhoto() {
		return this.prixPhoto;
	}

	public void setPrixPhoto(float prixPhoto) {
		this.prixPhoto = prixPhoto;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getUrlImage() {
		return this.urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public String getUrlMiniature() {
		return this.urlMiniature;
	}

	public void setUrlMiniature(String urlMiniature) {
		this.urlMiniature = urlMiniature;
	}

	public Vente getVente() {
		return this.vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

	public List<Tag> getTags() {
		return this.tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Theme> getThemes() {
		return this.themes;
	}

	public void setThemes(List<Theme> themes) {
		this.themes = themes;
	}

	public List<Format> getFormats() {
		return this.formats;
	}

	public void setFormats(List<Format> formats) {
		this.formats = formats;
	}

}