package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Statut_Commande database table.
 * 
 */
@Entity
@Table(name = "Statut_Commande")
public class StatutCommande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="statut_commande_id")
	private int statutCommandeId;

	private String libelle;

	//bi-directional many-to-one association to Commande
	@JsonIgnore
	@OneToMany(mappedBy="statutCommande")
	private List<Commande> commandes;

	public StatutCommande() {
	}

	public int getStatutCommandeId() {
		return this.statutCommandeId;
	}

	public void setStatutCommandeId(int statutCommandeId) {
		this.statutCommandeId = statutCommandeId;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setStatutCommande(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setStatutCommande(null);

		return commande;
	}

}