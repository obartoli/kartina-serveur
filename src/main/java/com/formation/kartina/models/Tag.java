package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tags database table.
 * 
 */
@Entity
public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tags_id")
	private int tagsId;

	private String tag;

	//bi-directional many-to-many association to Photographie
	@ManyToMany(mappedBy="tags")
	private List<Photographie> photographies;

	public Tag() {
	}

	public int getTagsId() {
		return this.tagsId;
	}

	public void setTagsId(int tagsId) {
		this.tagsId = tagsId;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public List<Photographie> getPhotographies() {
		return this.photographies;
	}

	public void setPhotographies(List<Photographie> photographies) {
		this.photographies = photographies;
	}

}