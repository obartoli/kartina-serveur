package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Theme database table.
 * 
 */
@Entity
public class Theme implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="theme_id")
	private int themeId;

	private String description;

	private String theme;

	//bi-directional many-to-many association to Photographie
	@ManyToMany(mappedBy="themes")
	@JsonIgnore
	private List<Photographie> photographies;

	public Theme() {
	}

	public int getThemeId() {
		return this.themeId;
	}

	public void setThemeId(int themeId) {
		this.themeId = themeId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTheme() {
		return this.theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public List<Photographie> getPhotographies() {
		return this.photographies;
	}

	public void setPhotographies(List<Photographie> photographies) {
		this.photographies = photographies;
	}

}