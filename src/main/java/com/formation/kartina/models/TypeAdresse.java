package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Type_Adresse database table.
 * 
 */
@Entity
@Table(name = "Type_Adresse")
public class TypeAdresse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="type_adresse_id")
	private int typeAdresseId;

	private String type;

	//bi-directional many-to-one association to Adresse
	@JsonIgnore
	@OneToMany(mappedBy="typeAdresse")
	private List<Adresse> adresses;

	public TypeAdresse() {
	}

	public int getTypeAdresseId() {
		return this.typeAdresseId;
	}

	public void setTypeAdresseId(int typeAdresseId) {
		this.typeAdresseId = typeAdresseId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Adresse> getAdresses() {
		return this.adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}

	public Adresse addAdress(Adresse adress) {
		getAdresses().add(adress);
		adress.setTypeAdresse(this);

		return adress;
	}

	public Adresse removeAdress(Adresse adress) {
		getAdresses().remove(adress);
		adress.setTypeAdresse(null);

		return adress;
	}

}