package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Type_Utilisateur database table.
 * 
 */
@Entity
@Table(name = "Type_Utilisateur")
public class TypeUtilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="type_utilisateur_id")
	private int typeUtilisateurId;

	private String type;

	//bi-directional many-to-one association to Utilisateur
	@JsonIgnore
	@OneToMany(mappedBy="typeUtilisateur")
	private List<Utilisateur> utilisateurs;

	public TypeUtilisateur() {
	}

	public int getTypeUtilisateurId() {
		return this.typeUtilisateurId;
	}

	public void setTypeUtilisateurId(int typeUtilisateurId) {
		this.typeUtilisateurId = typeUtilisateurId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Utilisateur> getUtilisateurs() {
		return this.utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	public Utilisateur addUtilisateur(Utilisateur utilisateur) {
		getUtilisateurs().add(utilisateur);
		utilisateur.setTypeUtilisateur(this);

		return utilisateur;
	}

	public Utilisateur removeUtilisateur(Utilisateur utilisateur) {
		getUtilisateurs().remove(utilisateur);
		utilisateur.setTypeUtilisateur(null);

		return utilisateur;
	}

}