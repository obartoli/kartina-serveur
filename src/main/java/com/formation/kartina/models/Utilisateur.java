package com.formation.kartina.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Utilisateur database table.
 * 
 */
@Entity
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="utilisateur_id")
	private int utilisateurId;

	private String biographie;

	private String civilite;

	private String email;

	@Column(name="est_bloque")
	private String estBloque;

	@Column(name="mot_de_passe")
	private String motDePasse;

	private String nom;

	private String prenom;

	private String telephone;

	@Column(name="url_fb")
	private String urlFb;

	@Column(name="url_pint")
	private String urlPint;

	@Column(name="url_twitter")
	private String urlTwitter;

	//bi-directional many-to-one association to Adresse
	@OneToMany(mappedBy="utilisateur", cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH})
	private List<Adresse> adresses;

	//bi-directional many-to-one association to Commande
	@JsonIgnore
	@OneToMany(mappedBy="utilisateur")
	private List<Commande> commandes;

	//bi-directional many-to-one association to Type_Utilisateur
	@ManyToOne
	@JoinColumn(name="type_utilisateur_id")
	private TypeUtilisateur typeUtilisateur;

	//bi-directional many-to-one association to Vente
	@JsonIgnore
	@OneToMany(mappedBy="utilisateur" )
	private List<Vente> ventes;

	public Utilisateur() {
	}

	public int getUtilisateurId() {
		return this.utilisateurId;
	}

	public void setUtilisateurId(int utilisateurId) {
		this.utilisateurId = utilisateurId;
	}

	public String getBiographie() {
		return this.biographie;
	}

	public void setBiographie(String biographie) {
		this.biographie = biographie;
	}

	public String getCivilite() {
		return this.civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEstBloque() {
		return this.estBloque;
	}

	public void setEstBloque(String estBloque) {
		this.estBloque = estBloque;
	}

	public String getMotDePasse() {
		return this.motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getUrlFb() {
		return this.urlFb;
	}

	public void setUrlFb(String urlFb) {
		this.urlFb = urlFb;
	}

	public String getUrlPint() {
		return this.urlPint;
	}

	public void setUrlPint(String urlPint) {
		this.urlPint = urlPint;
	}

	public String getUrlTwitter() {
		return this.urlTwitter;
	}

	public void setUrlTwitter(String urlTwitter) {
		this.urlTwitter = urlTwitter;
	}

	public List<Adresse> getAdresses() {
		return this.adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}

	public Adresse addAdress(Adresse adress) {
		getAdresses().add(adress);
		adress.setUtilisateur(this);

		return adress;
	}

	public Adresse removeAdress(Adresse adress) {
		getAdresses().remove(adress);
		adress.setUtilisateur(null);

		return adress;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setUtilisateur(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setUtilisateur(null);

		return commande;
	}

	public TypeUtilisateur getTypeUtilisateur() {
		return this.typeUtilisateur;
	}
	
	@JsonIgnore
	public String getRole() {
		return this.typeUtilisateur.getType();
	}

	public void setTypeUtilisateur(TypeUtilisateur typeUtilisateur) {
		this.typeUtilisateur = typeUtilisateur;
	}

	public List<Vente> getVentes() {
		return this.ventes;
	}

	public void setVentes(List<Vente> ventes) {
		this.ventes = ventes;
	}

	public Vente addVente(Vente vente) {
		getVentes().add(vente);
		vente.setUtilisateur(this);

		return vente;
	}

	public Vente removeVente(Vente vente) {
		getVentes().remove(vente);
		vente.setUtilisateur(null);

		return vente;
	}

}