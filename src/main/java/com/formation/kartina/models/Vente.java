package com.formation.kartina.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the Vente database table.
 * 
 */
@Entity
public class Vente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="vente_id")
	private int venteId;

	@Column(name="date_debut")
	private LocalDate dateDebut;

	@Column(name="date_fin")
	private LocalDate dateFin;

	@Column(name="nombre_exemplaire")
	private int nombreExemplaire;

	@Column(name="nombre_exemplaire_vendu")
	private int nombreExemplaireVendu;

	//bi-directional one-to-one association to Photographie
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name="photographie_id")
	private Photographie photographie;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="utilisateur_id")
	private Utilisateur utilisateur;

	//bi-directional many-to-one association to Ligne_Commande
	@JsonIgnore
	@OneToMany(mappedBy="vente")
	private List<LigneCommande> ligneCommandes;

	public Vente() {
	}

	public int getVenteId() {
		return this.venteId;
	}

	public void setVenteId(int venteId) {
		this.venteId = venteId;
	}

	public LocalDate getDateDebut() {
		return this.dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return this.dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public int getNombreExemplaire() {
		return this.nombreExemplaire;
	}

	public void setNombreExemplaire(int nombreExemplaire) {
		this.nombreExemplaire = nombreExemplaire;
	}

	public int getNombreExemplaireVendu() {
		return this.nombreExemplaireVendu;
	}

	public void setNombreExemplaireVendu(int nombreExemplaireVendu) {
		this.nombreExemplaireVendu = nombreExemplaireVendu;
	}

	public Photographie getPhotographie() {
		return this.photographie;
	}

	public void setPhotographie(Photographie photographie) {
		this.photographie = photographie;
	}

	public Utilisateur getUtilisateur() {
		return this.utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<LigneCommande> getLigneCommandes() {
		return this.ligneCommandes;
	}

	public void setLigneCommandes(List<LigneCommande> ligneCommandes) {
		this.ligneCommandes = ligneCommandes;
	}

	public LigneCommande addLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().add(ligneCommande);
		ligneCommande.setVente(this);

		return ligneCommande;
	}

	public LigneCommande removeLigneCommande(LigneCommande ligneCommande) {
		getLigneCommandes().remove(ligneCommande);
		ligneCommande.setVente(null);

		return ligneCommande;
	}

}