package com.formation.kartina.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.kartina.models.Commande;

public interface CommandeRepository extends JpaRepository<Commande, Integer>{
	
	Page<Commande> findAll(Pageable pageable);
	
}
