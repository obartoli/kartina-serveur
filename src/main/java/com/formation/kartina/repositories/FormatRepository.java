package com.formation.kartina.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.formation.kartina.models.Format;

public interface FormatRepository extends JpaRepository<Format, Integer>{
	
	@Query("SELECT DISTINCT f.nomFormat FROM Format f")
	List<String> findDistinctFormatName();
	
}
