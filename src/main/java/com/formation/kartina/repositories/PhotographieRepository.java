package com.formation.kartina.repositories;

import java.util.List;

import com.formation.kartina.models.Photographie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PhotographieRepository extends JpaRepository<Photographie, Integer>{
    
    @Query("SELECT DISTINCT p.orientation FROM Photographie p")
    List<String> findDistinctOrientation();
}