package com.formation.kartina.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.kartina.models.StatutCommande;

public interface StatutCommandeRepository extends JpaRepository<StatutCommande, Integer>{
	
	Optional<StatutCommande> findByLibelle(String libelle);
}
