package com.formation.kartina.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.formation.kartina.models.Theme;

public interface ThemeRepository extends JpaRepository<Theme, Integer>{
	
	@Query("SELECT DISTINCT t.theme FROM Theme t")
	List<String> findDistinctName();
	
}
