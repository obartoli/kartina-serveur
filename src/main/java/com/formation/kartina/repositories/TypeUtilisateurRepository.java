package com.formation.kartina.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.kartina.models.TypeUtilisateur;

public interface TypeUtilisateurRepository extends JpaRepository<TypeUtilisateur, Integer> {
	public Optional<TypeUtilisateur> findByType(String type);


}
