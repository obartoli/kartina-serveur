package com.formation.kartina.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.formation.kartina.models.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {

	@Query(" select u from Utilisateur u where u.email = ?1")
	Optional<Utilisateur> findByEmail(String mail);
	
	@Query ("select u from Utilisateur u Join u.typeUtilisateur tu where tu.typeUtilisateurId = ?1")
	public List<Utilisateur> getArtiste(Integer id);
	
	//@Query("UPDATE Utilisateur u SET u.biographie = '?' WHERE u.utilisateurId = ?1")
	//public Utilisateur editerBio(Integer id);
}
