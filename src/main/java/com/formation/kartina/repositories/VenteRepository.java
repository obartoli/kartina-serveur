package com.formation.kartina.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.formation.kartina.models.Vente;

public interface VenteRepository extends JpaRepository<Vente, Integer>{

	Page<Vente> findAll(Pageable pageable);

	@Query("SELECT v FROM Vente v " + 
			"JOIN v.photographie p " +
			"JOIN p.themes t " +
			"WHERE t.themeId = ?1")
	Page<Vente> findByTheme(Integer id, Pageable pageable);

	@Query("SELECT v FROM Vente v " +
			"WHERE v.nombreExemplaireVendu <= (v.nombreExemplaire / 10)")
	Page<Vente> findLastCopies(Pageable pageable);

	@Query("SELECT v FROM Vente v WHERE v.dateFin is null " + 
			"ORDER BY v.nombreExemplaireVendu DESC")
	Page<Vente> getBestVente(Pageable pageable);

	@Query("SELECT v FROM Vente v WHERE v.dateFin is null " + 
			"ORDER BY v.dateDebut DESC")
	List<Vente> getNewVente(Pageable pageable);

	@Query("SELECT v FROM Vente v JOIN v.utilisateur u WHERE u.utilisateurId = ?1")
	Page<Vente> getVenteByUtilisateur(Integer id, Pageable pageable);

	@Query("SELECT v FROM Vente v WHERE v.dateFin is null")
	Page<Vente> findCurrentSell(Pageable pageable);

	@Query("SELECT v FROM Vente v JOIN v.utilisateur u WHERE u.utilisateurId = ?1 AND v.dateFin is null "
			+ "ORDER BY v.dateDebut DESC")
	Page<Vente> getVenteCours(Integer id, Pageable pageable);

	@Query("SELECT DISTINCT v " + 
			"FROM Vente v " + 
			"JOIN v.photographie p " +
			"JOIN p.formats f " +
			"JOIN p.themes t " +
			"WHERE v.dateFin is null " +
			"AND p.orientation in ?1 " +
			"AND f.nomFormat in ?2 " +
			"AND t.theme in ?3")
	Page<Vente> findWithCriteria(List<String> orientations, List<String> formats, List<String> themes,Pageable pageable);
	
	@Query("SELECT v FROM Vente v JOIN v.utilisateur u WHERE u.utilisateurId = ?1 AND v.dateFin < CURRENT_DATE ORDER BY v.dateFin DESC")
	Page<Vente> getVentePassee(Integer id, Pageable pageable);
}
