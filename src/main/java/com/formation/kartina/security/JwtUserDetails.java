package com.formation.kartina.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtUserDetails implements UserDetails {
	
	private static final long serialVersionUID = 5126465035201968986L;
	
	private String username;
	private String prenom;
	private String password;
	
	private Collection<GrantedAuthority> authorities;
	
	public JwtUserDetails(String username, String password, Collection<GrantedAuthority> authorities, String prenom) {
		this.username = username;
		this.password = password;
		this.authorities = authorities;
		this.prenom = prenom;
		
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}
	
	public String getPrenom() {
		return this.prenom;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
