package com.formation.kartina.services;



import java.time.LocalDate;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.Commande;
import com.formation.kartina.models.LigneCommande;
import com.formation.kartina.models.StatutCommande;
import com.formation.kartina.repositories.CommandeRepository;


@Service
public class CommandeService {

	@Autowired
	CommandeRepository commandeRepository;

	@Autowired
	StatutCommandeService statutCommandeService;

	public Page<Commande> findAll(Pageable pageable) {
		return this.commandeRepository.findAll(pageable);
	}

	public Commande createOrUpdate(Commande commande) {
		return this.commandeRepository.save(commande);
	}

	public Commande createOrder(Commande commande) {

		if(commande.getLigneCommandes() != null) {
			for (LigneCommande ligneCommande : commande.getLigneCommandes()) {
				ligneCommande.setCommande(commande);
			}
		}

		StatutCommande satut = this.statutCommandeService.findByLibelle("En cours de préparation").get();
		commande.setStatutCommande(satut);
		commande.setDateCommande(LocalDate.now());
		commande.setNumeroFacture(UUID.randomUUID().toString());
		return this.commandeRepository.save(commande);
	}
}
