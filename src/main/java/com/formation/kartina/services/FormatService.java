package com.formation.kartina.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.Format;
import com.formation.kartina.repositories.FormatRepository;

@Service
public class FormatService {
	@Autowired
	private FormatRepository formatService;
	
	public List<Format> findAll()
	{
		return this.formatService.findAll();
	}
	
	public List<String> findDistinctFormatName() {
		return this.formatService.findDistinctFormatName();
	}
}
