package com.formation.kartina.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.Utilisateur;
import com.formation.kartina.repositories.UtilisateurRepository;
import com.formation.kartina.security.JwtUserDetails;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UtilisateurRepository repository;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public JwtUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Objects.requireNonNull(username);

		Utilisateur utilisateur = this.repository.findByEmail(username)	//username = email dans notre cas
				.orElseThrow(() -> new UsernameNotFoundException("User not Found"));
		
		return new JwtUserDetails(utilisateur.getEmail(), 
								  encoder.encode(utilisateur.getMotDePasse()), 
								  getGrantedAuthorities(utilisateur), 
								  utilisateur.getPrenom());
	}

	private Collection<GrantedAuthority> getGrantedAuthorities(Utilisateur utilisateur){

		Collection<GrantedAuthority> grantedAuthority = new ArrayList<>();

		if(utilisateur.getRole() != null) {
			if(utilisateur.getRole().equals("ADMIN")) {
				grantedAuthority.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			}

			if(utilisateur.getRole().equals("SELLER")) {
				grantedAuthority.add(new SimpleGrantedAuthority("ROLE_SELLER"));
			}

			if(utilisateur.getRole().equals("USER")) {
				grantedAuthority.add(new SimpleGrantedAuthority("ROLE_USER"));
			}
		}

		grantedAuthority.add(new SimpleGrantedAuthority("ROLE_VISITOR"));

		return grantedAuthority;
	}


}
