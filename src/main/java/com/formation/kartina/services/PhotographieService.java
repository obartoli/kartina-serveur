package com.formation.kartina.services;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.formation.kartina.models.Photographie;
import com.formation.kartina.models.Vente;
import com.formation.kartina.repositories.PhotographieRepository;

@Service
public class PhotographieService {

	@Autowired
	private PhotographieRepository photographieRepo;
	  
	public List<String> findDistinctOrientation()
	{
		return this.photographieRepo.findDistinctOrientation();
	}
	
	Logger log = LoggerFactory.getLogger(this.getClass().getName());
	private final String baseURL = "src/main/upload";
	private final String imageURL = "";
	private final Path rootLocation = Paths.get(baseURL + imageURL);

	public Photographie store(MultipartFile file) {
		try {
			Path imagePath = this.rootLocation.resolve(file.getOriginalFilename());
			System.out.println("path image: " + imagePath);
			Files.copy(file.getInputStream(), imagePath);
			BufferedImage image = ImageIO.read(imagePath.toFile());
			System.out.println("hauteur " + image.getHeight() + " " + "largeur " + image.getWidth());
			
			Photographie photo = new Photographie();
			String picURL = "http://localhost:8080/upload" + imageURL + "/" + file.getOriginalFilename();
			photo.setUrlImage(picURL);
			System.out.println(picURL);

			double ratio = image.getWidth() / (image.getHeight() * 1.0);
			System.out.println("RATIO ===========>");
			System.out.println(ratio*1.0);
			if (ratio == 1)
			{
				photo.setOrientation("Carré");
			}
			if (ratio < 1)
			{
				photo.setOrientation("Portrait");
			}
			if (ratio > 1 && ratio < 3)
			{
				photo.setOrientation("Paysage");
			}
			if (ratio >= 3)
			{
				photo.setOrientation("Panoramique");
			}
		
			System.out.println("orientation: " + photo.getOrientation());
			System.out.println("photo: " + photo);

			return photo;
		} catch (Exception e) {
			throw new RuntimeException("FAIL!");
		}
	}

	public Resource loadFile(String filename) {
		try {
			Path file = rootLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("FAIL!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("FAIL!");
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {
		try {
			Files.createDirectory(rootLocation);
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize storage!");
		}
	}
}
