package com.formation.kartina.services;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.StatutCommande;
import com.formation.kartina.repositories.StatutCommandeRepository;


@Service
public class StatutCommandeService {

	@Autowired
	StatutCommandeRepository statutCommandeRepository;

	public List<StatutCommande> findAll() {
		return this.statutCommandeRepository.findAll();
	}

	public StatutCommande createOrUpdate(StatutCommande statutCommande) {
		return this.statutCommandeRepository.save(statutCommande);
	}

	public Optional<StatutCommande> findById(Integer id) {
		return this.statutCommandeRepository.findById(id);
	}

	public Optional<StatutCommande> findByLibelle(String libelle) {
		return this.statutCommandeRepository.findByLibelle(libelle);
	}
}
