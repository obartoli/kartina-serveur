package com.formation.kartina.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.Theme;
import com.formation.kartina.repositories.ThemeRepository;

@Service
public class ThemeService {
	@Autowired
	private ThemeRepository themeRepo;
	
	public List<Theme>findAll()
	{
		return this.themeRepo.findAll();
	}
	
	public List<String> findDistinctName() {
		return this.themeRepo.findDistinctName();
	}
}
