package com.formation.kartina.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.TypeUtilisateur;
import com.formation.kartina.repositories.TypeUtilisateurRepository;

@Service
public class TypeUtilisateurService {
	@Autowired
	private TypeUtilisateurRepository typeUtilRepo;


	public Optional<TypeUtilisateur> findByType(String type) {
		return typeUtilRepo.findByType(type);
	}
}
