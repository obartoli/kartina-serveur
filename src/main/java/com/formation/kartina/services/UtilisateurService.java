package com.formation.kartina.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.Adresse;
import com.formation.kartina.models.TypeAdresse;
import com.formation.kartina.models.TypeUtilisateur;
import com.formation.kartina.models.Utilisateur;
import com.formation.kartina.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository repository;

	@Autowired
	private TypeUtilisateurService typeUtilService;

	@Autowired
	private UtilisateurService service;

	public Utilisateur createOrUpdate(Utilisateur utilisateur) {
		return this.repository.save(utilisateur);
	}

	public void delete(Utilisateur user) {
		this.repository.delete(user);
	}

	public List<Utilisateur> findAll() {
		return this.repository.findAll();
	}

	public Optional<Utilisateur> findById(Integer id) {
		return this.repository.findById(id);
	}

	public Optional<Utilisateur> findByUsername(String email) {
		return this.repository.findByEmail(email);
	}

	public List<Utilisateur> getArtiste(Integer id) {
		return this.repository.getArtiste(id);
	}

	public Utilisateur inscription(Utilisateur utilisateur){

		if (this.repository.findByEmail(utilisateur.getEmail()).isPresent()) {
			return null;
		}
		else {
			Optional<TypeUtilisateur> typeUtil= this.typeUtilService.findByType("USER");
			utilisateur.setTypeUtilisateur(typeUtil.get());
			TypeAdresse livraison = new TypeAdresse();
			livraison.setTypeAdresseId(1);
			for ( Adresse adresse : utilisateur.getAdresses()) {
				adresse.setUtilisateur(utilisateur);
				adresse.setTypeAdresse(livraison);
			}
			return this.service.createOrUpdate(utilisateur);	
		}
	}	

	public Utilisateur update(Utilisateur utilisateur){

		for ( Adresse adresse : utilisateur.getAdresses()) {
			adresse.setUtilisateur(utilisateur);
			if(adresse.getTypeAdresse() == null) {
				TypeAdresse livraison = new TypeAdresse();
				livraison.setTypeAdresseId(1);
				adresse.setTypeAdresse(livraison);
			}
		}
		return this.service.createOrUpdate(utilisateur);	

	}
}
