package com.formation.kartina.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.formation.kartina.models.Photographie;
import com.formation.kartina.models.Vente;
import com.formation.kartina.repositories.VenteRepository;

@Service
public class VenteService {

	@Autowired
	private VenteRepository venteRepo;
	
	@Autowired 
	private PhotographieService photographieService;
	
	@Autowired 
	private FormatService formatService;
	
	@Autowired 
	private ThemeService themeService;

	public Vente createOrUpdate(Vente vente) {
		return this.venteRepo.save(vente);
	}

	/*public Vente nvleVente(Vente vente, MultipartFile file)
	{
		Photographie photo = photographieService.store(file);
		vente.setDateDebut(new Date());
		vente.getPhotographie().setOrientation(photo.getOrientation());
		vente.getPhotographie().setUrlImage(photo.getUrlImage());
		vente.setUtilisateur(vente.getUtilisateur());
		vente.getPhotographie().setVente(vente);
		return createOrUpdate(vente);
	}*/
	
	public Page<Vente> getBestVente(Pageable pageable)
	{
		return this.venteRepo.getBestVente(pageable);
	}

	public List<Vente> getNewVente(Pageable pageable)
	{
		return this.venteRepo.getNewVente(pageable);
	}

	public Page<Vente >findAll(Pageable pageable)
	{
		return this.venteRepo.findAll(pageable);
	}
	
	public Page<Vente> findCurrentSell(Pageable pageable) {
		return this.venteRepo.findCurrentSell(pageable);
	}

	public Page<Vente> findByTheme(Integer id, Pageable pageable)
	{
		return this.venteRepo.findByTheme(id, pageable);
	}
	
	public Page<Vente> getVenteByUtilisateur(Integer id, Pageable pageable)
	{
		return this.venteRepo.getVenteByUtilisateur(id, pageable);
	}

	public Page<Vente> findLastCopies(Pageable pageable)
	{
		return this.venteRepo.findLastCopies(pageable);
	}

	public Vente getRandomVente(List<Vente> ventes) 
	{
		Random rand = new Random();
		Vente vente = null;

		if(!ventes.isEmpty()) {
			int index = rand.nextInt(ventes.size());
			vente = ventes.get(index);
		}

		return vente;
	}
	
	public Page<Vente> getVenteCours(Integer id, Pageable pageable) {
		return this.venteRepo.getVenteCours(id, pageable);
	}
	
	public Page<Vente> getVentePassee(Integer id, Pageable pageable) {
		return this.venteRepo.getVentePassee(id, pageable);
	}
	
	public Page<Vente> findWithCriteria(List<String> orientations, List<String> formats, List<String> themes, Pageable pageable) {
		if(orientations.isEmpty()) {
			orientations = this.photographieService.findDistinctOrientation();
			orientations.add("%");
		}
		if(formats.isEmpty()) {
			formats = this.formatService.findDistinctFormatName();
			formats.add("%");
		}
		if(themes.isEmpty()) {
			themes = this.themeService.findDistinctName();
			themes.add("%");
		}
		return this.venteRepo.findWithCriteria(orientations, formats, themes, pageable);
	}
	
	public Optional<Vente> getVenteById(Integer id) {
		return this.venteRepo.findById(id);
	}
}
