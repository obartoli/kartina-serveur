package com.formation.kartina.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.formation.kartina.models.Adresse;
import com.formation.kartina.models.Commande;
import com.formation.kartina.models.LigneCommande;
import com.formation.kartina.models.Utilisateur;

@Service
public class Mailer {

	@Autowired
	private JavaMailSender javaMailSender;
	
	//Envoie du récapitulatif de la commande
	public void sendMailOrder(Commande commande) throws MailException{
		
		Utilisateur utilisateur = commande.getUtilisateur();
		
		MimeMessagePreparator mail = mimeMessage -> {
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			
			messageHelper.setTo(utilisateur.getEmail());
			messageHelper.setFrom("kartina.sav@gmail.com");
			messageHelper.setSubject("Confirmation de votre commande kartina (#" + commande.getCommandeId() + ")");
			
			//Création du message
			String body = "<!DOCTYPE html>";
			body += "<html lang=\"fr\">";
			body += "<head></head>";
			
			//Corps du mail
			body += "<body style='font-size: 1.5em;'>";
			body += "<div style='padding: 10px;'>"
					+ "<h1>Bonjour " + utilisateur.getPrenom() + ",</h1>"
					+ "<span>Votre commande <b>#" + commande.getCommandeId() + "</b> est en cours de préparation !</span>"
					+ "</div>";
			
			//Rappel de l'adresse de livraison
			if(!utilisateur.getAdresses().isEmpty()) {
				Adresse livraison = utilisateur.getAdresses().get(0);
				body += "<div style='margin-top: 50px; margin-bottom: 50px; line-height: 5px;'>";
				body += "<h3 style='margin-bottom: 55px;'>Adresse de livraison</h3>";
				body += "<p><b>" + utilisateur.getNom() + " " + utilisateur.getPrenom() + "</b></p>";
				body += "<p>" + livraison.getRue() + "</p>";
				body += "<p>" + livraison.getVille() + ", " + livraison.getCodePostal() + "</p>";
				body += "<p>" + livraison.getPays() + "</p>";
				if(utilisateur.getTelephone() != null)
					body += "<p>" + utilisateur.getTelephone() + "</p>";
				body += "</div>";
			}
			
			// détail des produits commandés
			body += "<h3>Vos articles commandés</h3>";
			body += "<table style='width: 100%; border-collapse: collapse; text-align: center;'>";
			
			body += "<tr style='background-color: #40A458; color: white;'>";
			body += "<th> Photos </th>";
			body += "<th> Titre </th>";
			body += "<th> Quantité </th>";
			body += "<th> Prix photo </th>";
			body += "<th> Options </th>";
			body += "<th> Prix total </th>";
			body += "</tr>";
			
			for (LigneCommande produit : commande.getLigneCommandes()) {
				
				body += "<tr style='border-bottom: 2px solid lightgray'>";
				body += "<td><img src ='" + produit.getVente().getPhotographie().getUrlImage() + "' width='100' height='100'></td>";
				body += "<td>" + produit.getVente().getPhotographie().getTitre() + "</td>";
				body += "<td>" + produit.getQuantite() + "</td>";
				body += "<td>" + produit.getPrixUnitaire() + "€" + "</td>";
				body += "<td>" + "<b>Format : </b>" + produit.getFormat().getNomFormat() + "<br>";
				body += "<b>Finition : </b>" + produit.getFinition().getNomFinition() + "<br>";
				if(produit.getCadre() != null) {
					body += "<b>Cadre : </b>" + produit.getCadre().getNomCadre() + "</td>";
				}
				body += "<td style='color : #40A458; font-weight: 900;'>" + (produit.getPrixUnitaire() * produit.getQuantite()) + "€" + "</td>";
				body += "</tr>";
			}
			
			body += "</table>";
			
			body += "<div style='padding-right: 15px; float: right'>";
			body += "<h4>Total : " + commande.getPrixTtc() + "€</h4>";
			body += "</div>";
			body += "</body>";
			messageHelper.setText(body,true);
		};
		
		
		this.javaMailSender.send(mail);
		
	}

}
